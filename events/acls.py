from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    params = {"query": f"{city}, {state}", "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    photo = json.loads(response.content)
    try:
        return {"picture_url": photo["photos"][0]["src"]["original"]}
    except(IndexError, KeyError):
        return {"picture_url": None}
